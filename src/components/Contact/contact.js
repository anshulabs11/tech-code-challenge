import React, { useEffect, useState } from 'react';
import _ from 'lodash';

import Header from '../Header/header';
import './contacts.css';
import ContactList from './contactList';

const SearchInput = ({searchContacts}) => {

  return(
    <div className="form-group position-relative">
      <input className="form-control search-input bg-white" onChange={searchContacts} placeholder="Search contact here"/>
      <img src="../../img/search.png" alt="" className="search_icon"/>
    </div>
  )
  
}

const Contacts = () => {

  const [contacts, setContacts] = useState([]);
  const [searchedData, setSearchedData] = useState([]);

  useEffect(() => {
    fetchContacts()
    //eslint-disable-next-line
  }, []);

  //fetch contacts 
  const fetchContacts = async() => {
    // Where we're fetching data from
    fetch(`http://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json`)
      // We get the API response and receive data in JSON format...
      .then(response => response.json())
      // ...then we update the users state
      .then(data =>
        SortContacts(data)
      )
      // Catch any errors we hit and update the app
      .catch(error => console.log(error));
  }


  //Sort by last name
  const SortContacts = (data) =>{
    const sortedContact =  _.sortBy(data, ['obj', 'last_name']);
    setContacts(sortedContact)
  }

  //select contact and console
  const selectContact = (event, id) => {
    if (event.target.checked) console.log(id, 'ID of selected contact')
    _.set(_.find(contacts, {id: id}), 'checked', event.target.checked);
    SortContacts(contacts)
  }

 //filter according to user input 
 const searchContacts = (event) =>{
    if (event.target.value.length) {
      let filteredcontact = _.filter(contacts, function(o) {
          const name = o.first_name.toLowerCase()+" "+ o.last_name.toLowerCase()
          return name.includes(event.target.value.toLowerCase())
      });
      setSearchedData(filteredcontact)
    }
  }

  return (
    <React.Fragment>
      <Header />
      <section>
        <SearchInput searchContacts={searchContacts}/>
        <ContactList finalContacts={searchedData.length ? searchedData : contacts} selectContact={selectContact}/>
      </section>
    </React.Fragment>
  );
}

export default Contacts;
