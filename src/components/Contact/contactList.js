import _ from 'lodash';

import './contacts.css';

const ContactList = (props) => {
	const {finalContacts, selectContact} = props;
    //To get name if avatar is not exist
  const getAvatarName = (name) => {
    const fullName = name.split(' ');
    const initials = fullName.shift().charAt(0) + fullName.pop().charAt(0);
    return initials.toUpperCase();
  }

    return (
        <ul className="contact-list list-unstyled">
          {_.map(finalContacts, (contact, index) => {
              const name = _.get(contact,'first_name','')+" "+_.get(contact,'last_name','')
              const avatarName = getAvatarName(name)
              return (
                <li className="d-flex align-items-center border-bottom bg-light ps-3 py-2" key={index}>
                  <div className="contact-img">
                  {contact.avatar && <img src={contact.avatar} alt="" className="h-100 w-100"/>}
                  {!contact.avatar && <p className="mb-0 w-100 h-100 text-center avatar-name">{avatarName}</p>}
                  </div>
                  <div className="ms-3 flex-grow-1">
                    <h6 className="m-0">{name}</h6>
                    <p className="m-0">{_.get(contact,'email','')}</p>
                  </div>
                  <div className="form-check">
                    <input 
                      className="form-check-input" 
                      type="checkbox" 
                      value=""  
                      id="flexCheckDefault" 
                      onChange={(event)=>selectContact(event, contact.id)} 
                      checked={_.get(contact, 'checked', false)} 
                    />
                  </div>
              </li>
              )
            })}
        </ul>
    );
  
  }
  
  export default ContactList;
  