import './header.css'

const Header = () => {

  return (
      <header>
        <div className="navbar py-3 text-center">
          <h4 className="text-white text-uppercase fw-bold w-100 mb-0">Contacts</h4>
        </div>
      </header>
  );

}

export default Header;
