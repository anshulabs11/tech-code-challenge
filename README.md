# Tech Code Challenge

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Installing

- Make sure you have `yarn` and `node` installed (the recommended version is specified in the `engines` block of the `package.json`)
- Install the dependencies: `yarn`

## Running

 - Start the app: `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.
